GIT: https://bitbucket.org/vuprogrammers/delivery.git

Norint paleisti servisą reikia paleisiti komandas:

`docker build -t alpidifor/delivery:2 .`

`docker run --name companies -d -p 1234:1234 dancompany:3`
`docker run --link companies -d -p 80:7777 alpidifor/delivery:2`

`docker-compose up`

Rest servisas pasiekiamas:

Pradedant darba reikia prisijungti:

/login POST
{
"login" : "admin",
"password" : "admin"
}

/register POST
{
"login" : "admin",
"password" : "admin",
"permission" : "admin"
}

/users GET POST
/users/{id} GET PUT DELETE // DELETE works for admin only
/users/name/{name} GET

/products GET 
          POST
/products/{id} GET PUT DELETE
/products/name/{name} GET
/products/{id}/company GET  
/products/company/{id} GET 

/company/{id} GET PUT DELETE
/company POST

/orders GET POST
/orders/{id} GET PUT DELETE
/orders/user/{user ID} GET
/orders/product/{product ID} GET

USER:
{
	"name" : "Jack",
	"id" : 1,
	"address" : "Hm land 15-17",
	"phone" : "+37060000007"
}

PRODUCT:
{
	"id" : 1,
	"name" : "Wood",
	"price" : 12.5,
	"size" : "big"
	“company” : 1
}

ORDER:
{
	"id" : 1,
	"user_id" : 1,
	"sender_id" : 2,
	"transport" : "ship",
	"status" : "progress"
	"product_id" : 1,
	"quantity" : 250,
	"order_date" : "2017-05-04"
	"deadline_date" : "2017-06-01"
}

COMPANY:
{
    "companyId": 1,
    "insureEmployees": 13,
    "reviewRating": 6.5,
    "companyName": "UAB ",
    "foundedAt": "2015-02-15",
    "founder": "V.Stasys",
    "city": "Vilnius",
    "address": "Vilniaus g. 2",
    "email": "roklitas@inbox.lt",
    "phoneNumber": "+37065692001"
}
