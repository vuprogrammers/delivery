import spark.Request;
import spark.Response;

import java.util.List;

/**
 * Created by Edgar & Demid on 31/03/2017.
 */
public class ProductController {

    private static final int HTTP_BAD_REQUEST = 400;
    private static final int HTTP_NOT_FOUND = 404;

    public static Object getAllProducts(Request request, Response response, ProductData productData) {
        return productData.getAll();
    }

    public static Object getProduct(Request request, Response response, ProductData productData) {
        try {
            int id = Integer.valueOf(request.params("id"));
            Product product = productData.get(id);
            if (product == null) {
                throw new Exception("Nepavyko rasti");
            }
            if(product.getCompany() != -1){
                try{
                    Company company = JsonTransformer.fromJson(HttpConnectionHandler.send("GET","http://companies:1234/companies/"+product.getCompany(),null).toString(),Company.class);
                    return new ProductWithCompany(product, company);
                } catch (Exception e){
                    System.out.println("ERROR: " + e.getMessage());
                }
            }
            return product;
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko rasti prekes su id: " + request.params("id"));
        }
    }

    public static Object createProduct(Request request, Response response, ProductData productData) {
        try {
            Product product;
            try {
                product = JsonTransformer.fromJson(request.body(), Product.class);
            } catch (Exception e) {

                ProductWithCompany productWithCompany = JsonTransformer.fromJson(request.body(), ProductWithCompany.class);
                String company = JsonTransformer.toJson(productWithCompany.getCompany());
                System.out.println(company);
                String companyResult;
                product = new Product(productWithCompany);
                if (product.getName() == null || product.getPrice() == null || product.getSize() == null) {
                    response.status(HTTP_BAD_REQUEST);
                    return new ErrorMessage("Patikrinkyte duomenis");
                }
                try {
                    companyResult = HttpConnectionHandler.send("POST", "http://companies:1234/companies", company).toString();
                    System.out.println(companyResult);
                    product.setCompany(Integer.parseInt(companyResult.substring(companyResult.lastIndexOf(" ")+1, companyResult.length()-1)));
                } catch (Exception en) {
                    product.setCompany(-1);
                }

                productData.create(product);
                response.header("Product", "http://"+request.host()+ "/products/" + product.getId());
                if (product.getCompany() == -1){
                    response.status(202);
                    return new ErrorMessage("Napavyko prisijungti prie nuotolinio serverio, produktas sukurtas be kompanijos.");
                }
                return "OK";
            }

            if (product.getName() == null || product.getPrice() == null || product.getSize() == null) {
                response.status(HTTP_BAD_REQUEST);
                return new ErrorMessage("Patikrinkyte duomenis");
            }

            int company = product.getCompany();
            List<Company> companies;
            try {
                companies = JsonTransformer.listFromJson(HttpConnectionHandler.send("GET","http://companies:1234/companies", null)+"", Company.class);
            } catch (Exception e) {
                product.setCompany(-1);
                productData.create(product);
                response.header("Product", "http://"+request.host()+ "/products/" + product.getId());
                return "OK";
            }

            boolean founded = false;
            for (Company c : companies){
                if (c.getCompanyId() == company){
                    founded = true;
                    break;
                }
            }

            if (!founded){
                response.status(HTTP_BAD_REQUEST);
                return new ErrorMessage("Nepavyko rasti kompanijos su id: " + company);
            }

            productData.create(product);
            response.header("Product", "http://"+request.host()+ "/products/" + product.getId());
            return "OK";
        } catch (Exception e) {
            response.status(HTTP_BAD_REQUEST);
            System.out.println("PRODUCTCONTROLLER: BAD REQUEST");
            return new ErrorMessage("Patikrinkyte duomenis");
        }
    }

    public static Object updateProduct(Request request, Response response, ProductData productData) {
        try {
            int id = Integer.valueOf(request.params("id"));
            System.out.println("ID: " + id);
            Product product;
            try {
                product = JsonTransformer.fromJson(request.body(), Product.class);
            } catch (Exception e) {

                System.out.println("Read product with company: " + e.getMessage());
                ProductWithCompany productWithCompany = JsonTransformer.fromJson(request.body(), ProductWithCompany.class);
                String company = JsonTransformer.toJson(productWithCompany.getCompany());
                System.out.println(company);
                String companyResult;
                product = new Product(productWithCompany);
                try {
                    companyResult = HttpConnectionHandler.send("POST", "http://companies:1234/companies", company).toString();
                    System.out.println(companyResult);
                    product.setCompany(Integer.parseInt(companyResult.substring(companyResult.lastIndexOf(" ")+1, companyResult.length()-1)));
                } catch (Exception en) {
                    product.setCompany(-1);
                }

                productData.update(id,product);
                response.header("Product", "http://"+request.host()+ "/products/" + product.getId());
                if (product.getCompany() == -1){
                    response.status(202);
                    return  "Napavyko prisijungti prie nuotolinio serverio, produktas sukurtas be kompanijos.";
                }
                return "OK";
            }
            System.out.println("Can not found company data");
            int company = product.getCompany();
            List<Company> companies;
            try {
                companies = JsonTransformer.listFromJson(HttpConnectionHandler.send("GET","http://companies:1234/companies", null)+"", Company.class);
            } catch (Exception e) {
                product.setCompany(-1);
                productData.create(product);
                response.header("Product", "http://"+request.host()+ "/products/" + product.getId());
                return "OK";
            }

            boolean founded = false;
            for (Company c : companies){
                if (c.getCompanyId() == company){
                    founded = true;
                    break;
                }
            }

            if (!founded){
                response.status(HTTP_BAD_REQUEST);
                return new ErrorMessage("Nepavyko rasti kompanijos su id: " + company);
            }

            productData.update(id,product);
            response.header("Product", "http://"+request.host()+ "/products/" + product.getId());
            return "OK";
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko rasti prekes su id: " + request.params("id"));
        }
//        try {
//            Product product = JsonTransformer.fromJson(request.body(), Product.class);
//            int id = Integer.valueOf(request.params("id"));
//            productData.update(id, product);
//            response.header("Product", "http://"+request.host()+ "/products/" + product.getId());
//            return "OK";
//        } catch (Exception e) {
//            response.status(HTTP_NOT_FOUND);
//            return new ErrorMessage("Nepavyko rasti prekes su id: " + request.params("id"));
//        }
    }

    public static Object deleteProduct(Request request, Response response, ProductData productData) {
        try {
            int id = Integer.valueOf(request.params("id"));
            productData.delete(id);
            return "OK";
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko rasti prekes su id: " + request.params("id"));
        }
    }

    public static Object findProductByName(Request request, Response response, ProductData productData) {
        return productData.findByName(request.params("name"));
    }

    public static Object getProductCompany(Request request, Response response, ProductData productData) {
        try {
            int id = Integer.valueOf(request.params("id"));
            Product product = productData.getCompany(id);
            if (product == null) {
                System.out.println("product is null");
                throw new Exception();
            }
            // Request for company
            try {
                return HttpConnectionHandler.send("GET","http://companies:1234/companies/" + product.getCompany(), null);
            } catch (Exception e){
                response.status(HTTP_NOT_FOUND);
                return new ErrorMessage("Nepavyko rasti kompanijos");
            }

        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko rasti prekes su id: " + request.params("id"));
        }
    }

    public static Object getCompanyProducts(Request request, Response response, ProductData productData) {
        try {
            int companyId = Integer.valueOf(request.params("id"));
            List<Product> products = productData.getByCompany(companyId);
            if (products.isEmpty()) {
                response.status(HTTP_NOT_FOUND);
                return new ErrorMessage("Si kompanija neturi prekiu arba kompanija neegzistuoja");
            }
            return productData.getByCompany(companyId);
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko rasti companijos su id: " + request.params("id"));
        }
    }

}
