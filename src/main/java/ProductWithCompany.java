import lombok.Data;
/**
 * Created by edgar on 05/05/2017.
 */
@Data
public class ProductWithCompany {
    private int id;
    private String name;
    private Double price;
    private String size;
    private Company company;

    public ProductWithCompany(int id, String name, Double price, String size, Company company) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.size = size;
        this.company = company;
    }

    public ProductWithCompany(Product product, Company company) {
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.size = product.getSize();
        this.company = company;
    }

    public Company getCompany(){
        return company;
    }
}
