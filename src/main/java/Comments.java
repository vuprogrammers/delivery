/**
 * Created by Edgar & Demid on 31/03/2017.
 */
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static spark.Spark.*;

public class Comments {

    public static void main(String[] args) {
        UserData userData = new UserData();
        OrderData orderData = new OrderData();
        ProductData productData = new ProductData();

        port(7777);

        path("/users", () -> {
            get("", (req, res) -> {
                    return UserController.getAllUsers(req, res, userData);
                } , new JsonTransformer());

            get("/:id", (req, res) -> {
                return UserController.getUser(req, res, userData);
            } , new JsonTransformer());

            get("/name/:name", (req, res) -> {
                return UserController.findUserByName(req, res, userData);
            } , new JsonTransformer());

            post("", (req, res) -> {
                return UserController.createUser(req, res, userData);
            } , new JsonTransformer());

            put("/:id", (req, res) -> {
                return UserController.updateUser(req, res, userData);
            } , new JsonTransformer());

            delete("/:id", (req, res) -> {
                return UserController.deleteUser(req, res, userData);
            } , new JsonTransformer());

        });

        path("/products", () ->{
            get("", (req, res) -> {
                return ProductController.getAllProducts(req, res, productData);
            } , new JsonTransformer());

            get("/:id", (req, res) -> {
                return ProductController.getProduct(req, res, productData);
            } , new JsonTransformer());

            get("/name/:name", (req, res) -> {
                return ProductController.findProductByName(req, res, productData);
            } , new JsonTransformer());

            get("/:id/company", (req, res) -> {
                return ProductController.getProductCompany(req, res, productData);
            });

            get("/company/:id", (req, res) -> {
                return ProductController.getCompanyProducts(req, res, productData);
            } , new JsonTransformer());

            post("", (req, res) -> {
                return ProductController.createProduct(req, res, productData);
            } , new JsonTransformer());

            put("/:id", (req, res) -> {
                return ProductController.updateProduct(req, res, productData);
            } , new JsonTransformer());

            delete("/:id", (req, res) -> {
                return ProductController.deleteProduct(req, res, productData);
            } , new JsonTransformer());

        });

        path("/company", () ->{
            post("", WebController::createCompany);

            delete("/:id", WebController::deleteCompany);

            put("/:id", WebController::updateCompany);

            get("/:id", WebController::getCompany);
        });


        path("/orders", () ->{
            get("", (req, res) -> {
                return OrderController.getAllOrders(req, res, orderData);
            } , new JsonTransformer());

            get("/:id", (req, res) -> {
                return OrderController.getOrder(req, res, orderData);
            } , new JsonTransformer());

            get("/user/:userId", (req, res) -> {
                return OrderController.getOrdersByUser(req, res, orderData);
            } , new JsonTransformer());

            get("/product/:id", (req, res) -> {
                return OrderController.getOrdersByProduct(req, res, orderData);
            } , new JsonTransformer());

            post("", (req, res) -> {
                return OrderController.createOrder(req, res, orderData);
            } , new JsonTransformer());

            put("/:id", (req, res) -> {
                return OrderController.updateOrder(req, res, orderData);
            } , new JsonTransformer());

            delete("/:id", (req, res) -> {
                return OrderController.deleteOrder(req, res, orderData);
            } , new JsonTransformer());
        });

        exception(Exception.class, (e, req, res) -> {
            System.out.println(req.url());
            System.out.println(req.requestMethod());
            res.status(HTTP_BAD_REQUEST);
            JsonTransformer jsonTransformer = new JsonTransformer();
            res.body(jsonTransformer.render( new ErrorMessage(e) ));
        });

        after((req, rep) -> rep.type("application/json"));

    }
}
