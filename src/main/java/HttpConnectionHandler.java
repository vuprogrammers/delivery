/**
 * Created by edgar on 27/04/2017.
 */

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;


public class HttpConnectionHandler {

    private static final String USER_AGENT = "Mozilla/5.0";

    static Object send(String method, String url, String body) throws Exception {

        URL obj = new URL(url);
        System.out.println("Start connecting");
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        System.out.println("Connected");
        //add reuqest header
        con.setRequestMethod(method);
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        con.setConnectTimeout(3000);

        // Send request
        if (body != null) {
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.write(body.getBytes(StandardCharsets.UTF_8));
            wr.flush();
            wr.close();
        }


        int code = con.getResponseCode();
        System.out.println("CODE: " + code);
        BufferedReader in;
        if(code == 404){
            in = new BufferedReader(
                    new InputStreamReader(con.getErrorStream()));
        } else {
            in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
        }

        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response;
    }
}
