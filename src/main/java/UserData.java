import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by Edgar & Demid on 31/03/2017.
 */
public class UserData {

    private Map<Integer, User> users = new HashMap();

    UserData () {
        List<User> usersArray = Arrays.asList(
                new User(1,"Pirmas Useris", "Vilnius, kazkur 1 g.", "+37060000001"),
                new User(2,"Antras John", "Kaunas, kazkur 2 g.", "+37060000002"),
                new User(3,"Trecias Useris", "Klaipeda, kazkur 3 g.", "+37060000003"),
                new User(4,"Ketvirtas Useris", "Siauliai, kazkur 4 g.", "+37060000004"),
                new User(5,"Penktas Useris", "Vilnius, kazkur 5 g.", "+37060000005")
        );

        usersArray.forEach(
                (user) -> {this.users.put(user.getId(), user);
                });
    }

    public void create(User user) {
        user.setId(users.size()+1);
        users.put(user.getId(), user);
    }

    public void delete(int id) {
        users.remove(id);
    }

    public User get(int id) {
        return users.get(id);
    }

    public void update(int id, User user) {
        user.setId(id);
        users.put(id, user);
    }

    public List<User> getAll() {
        return users.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
    }

    public List<User> findByName(String name) {
        return users.entrySet().stream().filter(
                (entry) -> entry.getValue().getName().contains(name)
        ).map( Map.Entry::getValue ).collect(Collectors.toList());
    }
}
