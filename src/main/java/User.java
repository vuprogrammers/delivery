import lombok.Data;

/**
 * Created by Edgar & Demid on 31/03/2017.
 */
@Data
public class User {

    private int id;
    private String name;
    private String address;
    private String phone;

    public User(int id, String name, String address, String phone) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
    }


}
