import spark.Request;
import spark.Response;

import java.util.List;


/**
 * Created by edgar on 28/04/2017.
 */
public class WebController {

    private static final int HTTP_NOT_FOUND = 404;

    public static Object createCompany(Request request, Response response) {
        try {
            return HttpConnectionHandler.send("POST","http://companies:1234/companies", request.body());
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko prisijungti prie nutuolinio serverio");
        }
    }

    public static Object deleteCompany(Request request, Response response) {
        try {
            int companyId = Integer.valueOf(request.params("id"));
            return HttpConnectionHandler.send("DELETE","http://companies:1234/companies/" +companyId, null);
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko prisijungti prie nutuolinio serverio");
        }
    }

    public static Object updateCompany(Request request, Response response) {
        try {
            int companyId = Integer.valueOf(request.params("id"));
            return HttpConnectionHandler.send("PUT","http://companies:1234/companies/" +companyId, request.body());
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko prisijungti prie nutuolinio serverio");
        }
    }

    public static Object getCompany(Request request, Response response) {
        try {
            int companyId = Integer.valueOf(request.params("id"));
            return HttpConnectionHandler.send("GET","http://companies:1234/companies/" +companyId, null);
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko prisijungti prie nutuolinio serverio");
        }
    }
}
