import lombok.Data;

/**
 * Created by Edgar & Demid on 31/03/2017.
 */
@Data
public class Order {

    private int id;
    private int user_id;
    private int sender_id;
    private int product_id;
    private int quantity;
    private String order_date;
    private String transport;
    private String status;
    private String deadline_date;


    public Order(int id, int user_id, int sender_id, String transport, String status, int product_id, int quantity, String order_date, String deadline_date) {
        this.id = id;
        this.user_id = user_id;
        this.product_id = product_id;
        this.quantity = quantity;
        this.order_date = order_date;
        this.sender_id = sender_id;
        this.transport = transport;
        this.status = status;
        this.deadline_date = deadline_date;
    }

    public int getUserId(){
        return user_id;
    }

    public int getProductId() {
        return product_id;
    }
}
