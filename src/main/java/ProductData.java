import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Edgar & Demid on 31/03/2017.
 */
public class ProductData {

    private Map<Integer, Product> products = new HashMap();

    ProductData () {
        List<Product> usersArray = Arrays.asList(
                new Product(1,"Wood",16.99, "big", 1),
                new Product(2,"Nail",0.69, "small", 2),
                new Product(3,"Brick",0.25, "medium", 3),
                new Product(4,"Cement",9.99, "big",1),
                new Product(5,"Concrete mixer",169.99, "big", 1)
        );

        usersArray.forEach(
                (product) -> {this.products.put(product.getId(), product);
                });
    }

    public void create(Product product) {
        product.setId(products.size()+1);
        products.put(product.getId(), product);
    }

    public void delete(int id) {
        products.remove(id);
    }

    public Product get(int id) {
        return products.get(id);
    }

    public void update(int id, Product product) {
        product.setId(id);
        products.put(id, product);
    }

    public List<Product> getAll() {
        return products.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
    }

    public List<Product> findByName(String name) {
        return products.entrySet().stream().filter(
                (entry) -> entry.getValue().getName().contains(name)
        ).map( Map.Entry::getValue ).collect(Collectors.toList());
    }

    public Product getCompany(int id) {

        return products.get(id);
    }

    public List<Product> getByCompany(int id) {
        return products.entrySet().stream().filter(
                (entry) -> entry.getValue().getCompany() == id
        ).map( Map.Entry::getValue ).collect(Collectors.toList());
    }
}
