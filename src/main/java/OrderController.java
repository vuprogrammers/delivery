import spark.Request;
import spark.Response;

import java.util.List;

/**
 * Created by Edgar & Demid on 31/03/2017.
 */
public class OrderController {

    private static final int HTTP_BAD_REQUEST = 400;
    private static final int HTTP_NOT_FOUND = 404;

    public static Object getAllOrders(Request request, Response response, OrderData orderData) {
        return orderData.getAll();
    }

    public static Object getOrder(Request request, Response response, OrderData orderData) {
        try {
            int id = Integer.valueOf(request.params("id"));
            Order order = orderData.get(id);
            if (order == null) {
                throw new Exception("Nepavyko rasti");
            }
            return order;
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko rasti uzsakymo su id: " + request.params("id"));
        }
    }

    public static Object getOrdersByUser(Request request, Response response, OrderData orderData) {
        try {
            int userId = Integer.valueOf(request.params("userId"));
            List<Order> orders = orderData.findByUserId(userId);
            if (orders == null) {
                throw new Exception("Nepavyko rasti");
            }
            return orders;
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko rasti nei vieno uzsakymo su tokiu vartotojo id: " + request.params("userId"));
        }
    }

    public static Object getOrdersByProduct(Request request, Response response, OrderData orderData) {
        try {
            int id = Integer.valueOf(request.params("id"));
            List<Order> orders = orderData.findByProductId(id);
            if (orders == null) {
                throw new Exception("Nepavyko rasti");
            }
            return orders;
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko rasti nei vieno uzsakymo su tokiu prekes id: " + request.params("userId"));
        }
    }

    public static Object createOrder(Request request, Response response, OrderData orderData) {
        Order order = JsonTransformer.fromJson(request.body(), Order.class);
        orderData.create(order);
        response.header("Order", "http://"+request.host()+ "/orders/" + order.getId());
        return "OK";
    }

    public static Object updateOrder(Request request, Response response, OrderData orderData) {
        try {
            Order order = JsonTransformer.fromJson(request.body(), Order.class);
            int id = Integer.valueOf(request.params("id"));
            orderData.update(id, order);
            response.header("Order", "http://"+request.host()+ "/orders/" + order.getId());
            return "OK";
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko rasti uzsakymo su id: " + request.params("id"));
        }
    }

    public static Object deleteOrder(Request request, Response response, OrderData orderData) {
        try {
            int id = Integer.valueOf(request.params("id"));
            orderData.delete(id);
            return "OK";
        } catch (Exception e) {
            response.status(HTTP_NOT_FOUND);
            return new ErrorMessage("Nepavyko rasti uzsakymo su id: " + request.params("id"));
        }
    }
}
