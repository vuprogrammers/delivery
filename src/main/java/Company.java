import lombok.Data;

/**
 * Created by Edgar & Demid on 27/04/2017.
 */
@Data
public class Company {

    private int companyId;
    private int insureEmployees;
    private double reviewRating;
    private String companyName;
    private String foundedAt;
    private String founder;
    private String city;
    private String address;
    private String email;
    private String phoneNumber;

    public Company(int companyId,
                   int insureEmployees,
                   double reviewRating,
                   String companyName,
                   String foundedAt,
                   String founder,
                   String city,
                   String address,
                   String email,
                   String phoneNumber) {
        this.companyId = companyId;
        this.insureEmployees = insureEmployees;
        this.reviewRating = reviewRating;
        this.companyName = companyName;
        this.foundedAt = foundedAt;
        this.founder = founder;
        this.city = city;
        this.address = address;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

}
