import lombok.Data;

/**
 * Created by Edgar & Demid on 31/03/2017.
 */
@Data
public class Product {

    private int id;
    private String name;
    private Double price;
    private String size;
    private int company;

    public Product(int id, String name, Double price, String size, int company) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.size = size;
        this.company = company;
    }

    public int getCompany(){
        return company;
    }

    public Product(ProductWithCompany productWithCompany) {
        this.id = productWithCompany.getId();
        this.name = productWithCompany.getName();
        this.price = productWithCompany.getPrice();
        this.size = productWithCompany.getSize();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setCompany(int company) {
        this.company = company;
    }
}
