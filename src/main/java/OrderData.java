import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Edgar & Demid on 31/03/2017.
 */
public class OrderData {

    private Map<Integer, Order> orders = new HashMap();

    OrderData () {
        List<Order> usersArray = Arrays.asList(
                new Order(1,3, 1, "car", "In progress", 3, 15, "2017-01-01", "2017-02-01"),
                new Order(2,2, 1, "car", "In progress", 1, 20, "2017-01-02", "2017-03-01"),
                new Order(3,4, 1, "car", "In progress", 4, 12, "2017-01-03", "2017-04-01"),
                new Order(4,1, 1, "car", "In progress", 2, 7, "2017-01-04", "2017-05-01"),
                new Order(5,5, 1, "car", "In progress", 1, 70, "2017-01-05", "2017-06-01"),
                new Order(6,2, 1, "car", "In progress", 5, 17, "2017-01-06", "2017-07-01"),
                new Order(7,3, 1, "car", "In progress", 1, 1, "2017-01-07", "2017-08-01")

        );

        usersArray.forEach(
                (order) -> {this.orders.put(order.getId(), order);
                });
    }

    public void create(Order order) {
        order.setId(orders.size()+1);
        orders.put(order.getId(), order);
    }

    public void delete(int id) {
        orders.remove(id);
    }

    public Order get(int id) {
        return orders.get(id);
    }

    public void update(int id, Order order) {
        order.setId(id);
        orders.put(id, order);
    }

    public List<Order> getAll() {
        return orders.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
    }

    public List<Order> findByUserId(int id) {
        return orders.entrySet().stream().filter(
                (entry) -> entry.getValue().getUserId() == id
        ).map( Map.Entry::getValue ).collect(Collectors.toList());
    }

    public List<Order> findByProductId(int id) {
        return orders.entrySet().stream().filter(
                (entry) -> entry.getValue().getProductId() == id
        ).map( Map.Entry::getValue ).collect(Collectors.toList());
    }

}
