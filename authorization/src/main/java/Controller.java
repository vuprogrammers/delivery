import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by edgar on 25/05/2017.
 */
public class Controller {

    ArrayList<User> users = new ArrayList<>();

    Controller() {
        User u1 = new User(0, "admin", "admin",UUID.randomUUID().toString(), "admin");
        User u2 = new User(1, "user", "12345",UUID.randomUUID().toString(), "user");
        users.add(u1);
        users.add(u2);
    }

    public Object redirect(Request req, Response res) {
        String token = req.headers("Authorization");
        if (token != null) {
            for (User user : users) {
                if (token.equals(user.getToken())) {
                    try {
                        System.out.println("METHOD: " + req.requestMethod());
                        return HttpConnectionHandler.send(req.requestMethod(),"http://delivery:7777" + req.pathInfo(), req.body(), res);
                    } catch (Exception e) {
                        res.status(400);
                        return "Hmm...";
                    }
                }
            }
        }

        res.status(401);
        return "{ \"message\" : \"Prasome prisijungti.\"}";
    }

    public Object redirectForAdmin(Request req, Response res) {
        String token = req.headers("Authorization");
        if (token != null) {
            for (User user : users) {
                if (token.equals(user.getToken()) && user.getPermission().equals("admin")) {
                    try {
                        System.out.println("METHOD: " + req.requestMethod());
                        return HttpConnectionHandler.send(req.requestMethod(),"http://delivery:7777" + req.pathInfo(), req.body(),res);
                    } catch (Exception e) {
                        res.status(400);
                        return "Hmm...";
                    }
                }
            }
        }

        res.status(401);
        return "{ \"message\" : \"Prasome prisijungti.\"}";
    }

    public Object login(Request request, Response response) {
        try {
            User user = JsonTransformer.fromJson(request.body(), User.class);
            for (User u : users) {
                if (u.getLogin().equals(user.getLogin()) && u.getPassword().equals(user.getPassword())) {
                    u.setToken(UUID.randomUUID().toString());
                    return u;
                }
            }
        } catch (Exception ex) {

        }
        response.status(400);
        return new ErrorMessage("Nepavyko prisijungti");

    }

    public Object register(Request request, Response response) {

        try {
            User user = JsonTransformer.fromJson(request.body(), User.class);
            if(user.getPassword().isEmpty() || user.getLogin().isEmpty() ||
                    !(user.getPermission().equals("admin") || user.getPermission().equals("user"))
                    ){
                return new ErrorMessage("Patikrinkite ivedimo duomenis. Vartotojas neatpazintas.");
            }

            for (User u : users) {
                if (u.getLogin().equals(user.getLogin()) ) {
                    response.status(400);
                    return new ErrorMessage("Toks vartotojo vardas jau uzimtas.");
                }
            }

            user.setId(users.size());
            user.setToken(UUID.randomUUID().toString());

            users.add(user);
            return user;

        } catch (Exception ex) {

        }
        response.status(400);
        return new ErrorMessage("Nepavyko prisiregistruoti");

    }
}
