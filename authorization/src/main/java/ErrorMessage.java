import lombok.Data;

/**
 * Created by Edgar & Demid on 31/03/2017.
 */
@Data
public class ErrorMessage {
    private String message;

    public ErrorMessage(String message) {
        this.message = message;
    }

    public ErrorMessage(String message, String... args) {
        this.message = String.format(message, args);
    }

    public ErrorMessage(Exception e) {
        this.message = e.getMessage();
    }
}
