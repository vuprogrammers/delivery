/**
 * Created by edgar on 25/05/2017.
 */
public class User {

    private int id;
    private String login;
    private String password;
    private String token;
    private String permission;

    public User(int id, String login, String password , String token, String permission) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.token = token;
        this.permission = permission;
    }

    public String getLogin() {
        return login;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
