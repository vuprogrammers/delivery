/**
 * Created by Edgar & Demid on 31/03/2017.
 */
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static spark.Spark.*;

public class Distributor {

    public static void main(String[] args) {
        Controller controller = new Controller();

        port(1111);

        path("/login", () ->{
            post("", controller::login, new JsonTransformer());
        });

        path("/register", () ->{
            post("", controller::register, new JsonTransformer());
        });

        path("/users", () -> {
            get("", controller::redirect);

            get("/:id", controller::redirect);

            get("/name/:name", controller::redirect);

            post("", controller::redirect);

            put("/:id", controller::redirect);

            delete("/:id",controller::redirectForAdmin);

        });

        path("/products", () ->{
            get("", controller::redirect);

            get("/:id", controller::redirect);

            get("/name/:name", controller::redirect);

            get("/:id/company", controller::redirect);

            get("/company/:id", controller::redirect);

            post("", controller::redirect);

            put("/:id", controller::redirect);

            delete("/:id", controller::redirect);

        });

        path("/company", () ->{
            post("", controller::redirect);

            delete("/:id", controller::redirect);

            put("/:id", controller::redirect);

            get("/:id", controller::redirect);
        });


        path("/orders", () ->{
            get("", controller::redirect);

            get("/:id", controller::redirect);

            get("/user/:userId", controller::redirect);

            get("/product/:id", controller::redirect);

            post("", controller::redirect);

            put("/:id", controller::redirect);

            delete("/:id", controller::redirect);
        });

        exception(Exception.class, (e, req, res) -> {
            res.status(HTTP_BAD_REQUEST);
            JsonTransformer jsonTransformer = new JsonTransformer();
            res.body(jsonTransformer.render( new ErrorMessage(e) ));
        });

        after((req, rep) -> rep.type("application/json"));

    }
}
